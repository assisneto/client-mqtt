
// chamando a instancia do db
const mongoose = require('mongoose');

// chamando o modelo de dados que vai ser salvo no bd
const data = require('./models/rasp');

// url que vai ser utilizada para salvar os dados
const dbUrl = 'mongodb://localhost:27017/got';

// chamando a classe que vai ser responsavel por salvar os dados no banco de dados
const test = require('./config/connectionFactory.js');

// instanciando a classe 
const db = new test(dbUrl,mongoose,data);
let start = undefined;
let end = undefined;
let ligado = false;

const mqtt = require('mqtt')
const client  = mqtt.connect('mqtt://192.168.0.101',{
  port:1883,
  clientId:'coletorDeDados',
  username:'lara',
  password:'lara123',
});


client.on('connect', function () {
  console.log("Conectou ao broker");
  client.subscribe('nodemcu/ar/state');
})

client.on('message', function (topic, message) {
  console.log(`[${topic}] - ${message}`);
  if(topic == 'nodemcu/ar/state'){
    if(message == 'ligado' && ligado == false){
      console.log(ligado);
      ligado = true;
      console.log(ligado);
      start = new Date;
      console.log('Iniciou contador');
    }else if(message == 'desligou' && ligado == true) {
      ligou = false;
      end = new Date;
      diff = end - start;
      console.log(`Fim da contagem. Diferenca: ${diff}`);
      db.salvar(diff, 0);
    }
  }
});
 