class connectionFactory {
  constructor(connection, mongo, ...args) {
      this._connection = connection;
      this._args = args;
      this._mongo = mongo.connect(
      this._connection,
        {useNewUrlParser:true,}
        );
      console.log('Conectou no Banco de Dados');
  }
  salvar(message,NumberModel){
    const dados = new this._args[NumberModel]({content: message});
    console.log(message.toString());
    dados.save((err,results)=>{
    if (err) throw err;
      console.log('Dados salvos com sucesso!', results);
  });
  }
}

module.exports = connectionFactory;
