const mongoose = require('mongoose');

const raspSchema = new mongoose.Schema({
  content: Number,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('rasp', raspSchema);